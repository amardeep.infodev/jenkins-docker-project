package com.example.jenkinsdockerproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JenkinsDockerProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(JenkinsDockerProjectApplication.class, args);
        System.out.println("hello world");
    }

}
